var nama = "Junaedi";
var peran = "Werewolf";
if (nama === "") {
  console.log("Nama Harus Diisi");
} else {
  console.log("Selamat datang di Dunia Werewolf," + nama);
  if (peran === "Penyihir") {
    console.log(
      "Halo Penyihir " +
        nama +
        ",kamu dapat melihat siapa yang menjadi werewolf!"
    );
  } else if (peran === "Guard") {
    console.log(
      "Halo Guard " +
        nama +
        ", kamu akan membantu melindungi temanmu dari serangan werewolf."
    );
  } else if (peran === "Werewolf") {
    console.log(
      "Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!"
    );
  }
}
console.log();

var tahun = 1945;
var bulan = 5;
var tanggal = 1;

switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
}
console.log(tanggal + " " + bulan + " " + tahun);

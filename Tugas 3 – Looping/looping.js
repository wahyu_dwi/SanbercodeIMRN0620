console.log("\n LOOPING PERTAMA");
var i = 1;
while (i < 22) {
  if ((i % 2) == 0) {
    console.log(i + " - I Love Coding");
  }
  i++;
}

console.log("\n LOOPING KEDUA");
var j = 20;
while (j > 0) {
  if ((j % 2) == 0) {
    console.log(j + " - I will become a mobile developer");
  }
  j--;
}
console.log("\n Looping menggunakan for");

for (i = 1; i <= 20; i++) {
  if ((i % 2) == 1 && (i % 3) == 1) {
    console.log(i + " - Santai");
  } else if ((i % 2) == 1 && (i % 3) == 0) {
    console.log(i + "- I Love Coding");
  }
  else if ((i % 2) == 0) {
    console.log(i + "- Berkualitas");
  } else {
    console.log(i + " - Santai");
  }
}

console.log("\n No. 3 Membuat Persegi Panjang");


var m = "";

for (i = 1; i <= 4; i++) {
  for (j = 1; j <= 8; j++) {
    m += "#";
  }
  m += "\n";
}

console.log(m);


console.log("\n No. 4 Membuat Tangga");
var s = "";

for (i = 1; i <= 7; i++) {
  for (j = 1; j <= i; j++) {
    s += "#";
  }
  s += "\n";
}

console.log(s);


console.log(" \n No. 5 Membuat Papan Catur");

var z = "";
var kolom = 8;
var baris = 8;
var x = 1;
while (x <= baris) {
  var y = 1;
  while (y <= kolom) {
    if ((x + y) % 2 > 0) {
      z += "#";
    } else {
      z += " ";
    }
    y++;
  }
  x++;
  z += "\n";

}
console.log(z);

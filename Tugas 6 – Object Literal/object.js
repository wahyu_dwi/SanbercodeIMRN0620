// Soal No. 1 (Array to Object)
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    var personObj = {}
    if (arr.length != 0) {
        for (var i = 0; i < arr.length; i++) {
            personObj.firstName = String(arr[i][0])
            personObj.lastName = String(arr[i][1])
            personObj.gender = String(arr[i][2])
            if ((arr[i][3]) && (arr[i][3] < thisYear)) {
                personObj.age = String(thisYear - arr[i][3])
            }
            else {
                personObj.age = "Invalid birth year"
            }
            console.log(i + 1, ".", personObj.firstName, personObj.lastName, ":", personObj)
        }
    }
    else
        console.log('""')
}


// Driver Code
console.log("NOMOR 1 ARRAY TO OBJECT\n")
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""









function shoppingTime(memberId, money) {
    // you can only write your code here!

    if (memberId == null) {
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja");
    } else if (money <= 50000) {
        console.log("Mohon maaf, uang tidak cukup");
    }
    function showProps(obj, objName) {
        var result = ``;
        for (var i in obj) {
            // obj.hasOwnProperty() is used to filter out properties from the object's prototype chain
            if (obj.hasOwnProperty(i)) {
                result += `${objName}.${i} = ${obj[i]}\n`;
            }
        }
        return result;
    }
}



// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja












